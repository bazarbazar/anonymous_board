package bazarbazaraps.anonymous_board

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_main.*
import android.text.Editable
import android.text.TextWatcher
import com.mancj.materialsearchbar.MaterialSearchBar
import kotlinx.android.synthetic.main.row_main.view.*
import android.widget.TextView
import android.widget.Toast
import android.speech.RecognizerIntent
import android.content.Intent
import android.graphics.Color
import android.view.inputmethod.InputMethodManager
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList
import com.startapp.android.publish.adsCommon.StartAppSDK

class MainActivity : AppCompatActivity() {
    private val REQUEST_CODE = 1234
    private lateinit var database: DatabaseReference


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        StartAppSDK.init(this, "203489161", true);
        //****************************
        //***UI
        //****************************
        setContentView(R.layout.activity_main)

        val send_message = findViewById<ToggleButton>(R.id.send_button)
        val textMessage= findViewById<TextView>(R.id.text_message)

        val searchBar = findViewById(R.id.searchBar) as MaterialSearchBar
        searchBar.setHint("Search...")
        searchBar.setSpeechMode(true)

        val listView = findViewById<ListView>(R.id.samples_list_view)

        //****************************
        //***FireBase connection
        //****************************
        database = FirebaseDatabase.getInstance().reference.child("posts")



        val SAMPLE_NAMES_CONST = arrayListOf<ListContainer>()

        val arrayAdapter = MyCustomAdapter(this, SAMPLE_NAMES_CONST)

        listView.setAdapter(arrayAdapter)
        arrayAdapter.filter.filter("")


        //send_message.setOnClickListener(){
         //
        //}

        send_button.setOnCheckedChangeListener { buttonView, isChecked ->

                // The toggle is enabled/checked
                if(textMessage.text.toString().length<500){
                    if((textMessage.text.toString().equals("Anonymous:"))||(textMessage.text.toString().isBlank())|| (Pattern.matches("[\\s]+", textMessage.text.toString()))||(textMessage.text.toString().equals(""))||(textMessage.text.toString().equals(" "))||(textMessage.text.toString().equals("  "))) {
                        Toast.makeText(this,"Err: Enter a text...",Toast.LENGTH_LONG).show()
                        textMessage.setText("")
                    }else{

                        val key = database.child("posts").push().key

                        database.child(key.toString()).child("string").setValue(textMessage.text.toString())
                        database.child(key.toString()).child("time").setValue(Date().getTime().toString())
                        textMessage.setText("")

                        val view = this.currentFocus
                        view?.let { v ->
                            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
                            imm?.let { it.hideSoftInputFromWindow(v.windowToken, 0) }
                        }
                        Toast.makeText(this,"Message published.",Toast.LENGTH_LONG).show()
                    }
                }else{Toast.makeText(this,"Max message lenth 500char...",Toast.LENGTH_LONG).show()}

                arrayAdapter.notifyDataSetChanged();
                listView.smoothScrollToPosition(arrayAdapter.getCount()-1);



                //send_button.setBackgroundResource(R.drawable.button_anony)



        }


        listView.setOnItemClickListener{parent, view, position, id ->

            val layout =   view.sampletextView
            val params = layout.layoutParams







            if (arrayAdapter.get_clicked(position)==false) {



                layout.setBackgroundColor(getResources().getColor(R.color.colorSearchBar))
                if(layout.lineCount!=1){


                    params.height = (layout.lineHeight*layout.lineCount)+20
                    layout.setLayoutParams(params);

                    if ((position == listView.getCount() - 1)) {
                        listView.setSelection(position);

                    }

                }else{
                //nothing
                }




                arrayAdapter.set_clicked(position,true)
            }else {
                layout.setBackgroundColor(Color.WHITE)
                if (layout.lineCount != 1) {
                    params.height = layout.minHeight;
                    layout.setLayoutParams(params);

                   if ((position == listView.getCount() - 1)) {
                        listView.setSelection(position);

                    }



                }
                arrayAdapter.set_clicked(position, false)
            }

        }

        fun startVoiceRecognitionActivity()
        {
            val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Voice searching...");
            startActivityForResult(intent, REQUEST_CODE);
        }

        searchBar.setOnSearchActionListener(object : MaterialSearchBar.OnSearchActionListener {
            override fun onSearchConfirmed(text: CharSequence?) {
               // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onSearchStateChanged(enabled: Boolean) {
               // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onButtonClicked(buttonCode: Int) {
                when (buttonCode) {

                    1 -> {
                        Toast.makeText(searchBar.context, "Voice search in future...", Toast.LENGTH_LONG).show()
                        startVoiceRecognitionActivity()
                    }
                    2 -> {
                        //Toast.makeText(searchBar.context, "Max długość wiadomości 500zn.", Toast.LENGTH_LONG).show()
                    }
                }
            }
            })

        searchBar.addTextChangeListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                arrayAdapter.filter.filter(charSequence)
            }


            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                //SEARCH FILTER
                arrayAdapter.filter.filter(charSequence)
            }


            override fun afterTextChanged(editable: Editable) {
            }
        })


        val postListener = object : ChildEventListener {

            override fun onChildAdded(datasnapshot: DataSnapshot, string: String?) {

                val post = datasnapshot.value

                if (post != null) {
                    val post_string= datasnapshot.child("string").getValue(String::class.java)
                    val post_time= datasnapshot.child("time").getValue(String::class.java)
                    val  listcontainer =ListContainer()
                    listcontainer.clicked=false
                    listcontainer.post=post_string.toString()
                    listcontainer.time=post_time.toString()
                    SAMPLE_NAMES_CONST.add(listcontainer)



                    arrayAdapter.notifyDataSetChanged()


                }

            }
            override fun onChildChanged(dataSnapshot: DataSnapshot, previousChildName: String?) {


                // [END_EXCLUDE]
            }

            override fun onChildRemoved(dataSnapshot: DataSnapshot) {

            }

            override fun onChildMoved(dataSnapshot: DataSnapshot, previousChildName: String?) {

            }

            override fun onCancelled(databaseError: DatabaseError) {

            }

        }

        ///****************************
        //*** Load posts from FireBase
        //****************************
        database.limitToLast(500).addChildEventListener(postListener)
        arrayAdapter.notifyDataSetChanged()
        listView.setSelection(arrayAdapter.getCount() - 1);
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {

        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK)
        {
            // Populate the wordsList with the String values the recognition engine thought it heard
            var matches = ArrayList<String>()
            matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
            if (!matches.isEmpty())
            {
                val query = matches?.get(0);
                searchBar.text=query
                searchBar.performClick()
                //speak.setEnabled(false)

            }
        }
        //super.onActivityResult(requestCode, resultCode, data)
        // Check which request we're responding to


    }

}





