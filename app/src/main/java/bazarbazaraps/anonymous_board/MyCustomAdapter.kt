package bazarbazaraps.anonymous_board

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import java.text.SimpleDateFormat

class MyCustomAdapter(context: Context?, private var sample_names_const: ArrayList<ListContainer>) : ArrayAdapter<ListContainer>(context, -1, sample_names_const){

    var filtered = ArrayList<ListContainer>()

    var  listcontainer =ListContainer()

    private var mContext = context
    init{

        mContext = context

        for (sample_name in sample_names_const) {
            //listcontainer.clicked=false
            listcontainer.post=sample_name.post
            filtered.add(listcontainer)

        }
    }

    fun get_post(position: Int): String {
        return filtered[position].post // or maybe mList.get(position);
    }
    fun get_clicked(position: Int): Boolean {
        return filtered[position].clicked // or maybe mList.get(position);
    }
    fun set_clicked(position: Int, value: Boolean) {
        filtered[position].clicked=value // or maybe mList.get(position);
    }
    override fun getCount(): Int {
        //return sample_names_const.size
        return filtered.size
    }
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {




        var layoutInflater = LayoutInflater.from(mContext)
        var rowMain= layoutInflater.inflate(R.layout.row_main, parent, false)
        var sampleTextView = rowMain.findViewById<TextView>(R.id.sampletextView)
        var datetextView = rowMain.findViewById<TextView>(R.id.datetextView)
        //sampleTextView.text = sample_names_const[position]
        sampleTextView.text = filtered[position].post
        val dateFormat = SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
        //dateFormat.format(Date().getTime())
        datetextView.text = dateFormat.format(filtered[position].time.toLong())
        //sampleTextView.setMovementMethod(ScrollingMovementMethod())
        filtered[position].clicked=listcontainer.clicked

        return rowMain
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    //override fun getItem(position: Int): ListContainer? {
    //   return "test"
    //}

    override fun getFilter()= filter

    private var filter: Filter = object : Filter() {

        override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {
            val results = FilterResults()

            val query = if (constraint != null && constraint.isNotEmpty()&& constraint.length>0) {
                autocomplete(constraint.toString())

            }
            else {
                sample_names_const

            }
            results.values = query
            results.count = query.size
            return results
        }

        private fun autocomplete(input: String): ArrayList<ListContainer> {
            var results = arrayListOf<ListContainer>()

            for (sample_name in sample_names_const) {

                //listcontainer.clicked=false
                //listcontainer.post=sample_name.post
                if (sample_name.post.toLowerCase().contains(input)) results.add(sample_name)

            }


            return results
        }

        override fun publishResults(constraint: CharSequence?, results: Filter.FilterResults) {
            filtered = results.values as ArrayList<ListContainer>
            notifyDataSetChanged();
        }

        //override fun convertResultToString(result: Any) = (result as ArrayList<ListContainer>)
    }

}